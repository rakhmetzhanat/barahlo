package com.example.zhanat.store.Adapter;
/**
 * Created by zhanat on 4/14/16.
 */

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.zhanat.store.Constructor.MessagesConstructor;
import com.example.zhanat.store.R;

import java.util.ArrayList;

public class MessagesAdapter extends BaseAdapter {
    TextView textView26;
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<MessagesConstructor> objects;

    public MessagesAdapter(Context context, ArrayList<MessagesConstructor> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = lInflater.inflate(R.layout.messages, parent, false);
        if (view == null) {
            view = lInflater.inflate(R.layout.messages, parent, false);
        }

        MessagesConstructor messagesConstructor = getMessagesConstructor(position);

        textView26 = (TextView) view.findViewById(R.id.textView26);
        textView26.setText(messagesConstructor.content);

        if(messagesConstructor.from.equals("user"))
        {
            textView26.setText(messagesConstructor.content);
            textView26.setGravity(Gravity.RIGHT);
            textView26.setBackgroundResource(R.drawable.border_2);

        }else if(messagesConstructor.from.equals("customer"))
        {
            textView26.setText(messagesConstructor.content);
            textView26.setGravity(Gravity.LEFT);
            textView26.setBackgroundResource(R.drawable.border);

        }

        return view;
    }

    MessagesConstructor getMessagesConstructor(int position) {
        return ((MessagesConstructor) getItem(position));
    }
}