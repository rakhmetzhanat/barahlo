package com.example.zhanat.store.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.zhanat.store.Adapter.ItemsAdapterPop;
import com.example.zhanat.store.Constructor.ItemsConstructorPop;
import com.example.zhanat.store.R;
import com.example.zhanat.store.ScrollingActivity;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.github.rahatarmanahmed.cpv.CircularProgressView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentPop#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentPop extends Fragment{

    ArrayList<ItemsConstructorPop> itemsConstructorPop = new ArrayList<ItemsConstructorPop>();
    ItemsAdapterPop itemsAdapterPop;


    public Firebase ref;

    public String name;
    public String unit_price;
    public String customers_idcustomers;
    public String description;
    public String stock;
    public String id;

    ListView listView;
    public int idproducts;
    CircularProgressView progressView;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public FragmentPop() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPop.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentPop newInstance(String param1, String param2) {
        FragmentPop fragment = new FragmentPop();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(getContext());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Firebase.setAndroidContext(getContext());
        ref = new Firebase("https://flickering-heat-3014.firebaseio.com");
        View view = inflater.inflate(R.layout.fragment_fragment_pop, container,false);
        listView = (ListView) view.findViewById(R.id.listView);

        progressView = (CircularProgressView) view.findViewById(R.id.progress_view);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long arg3) {
                Intent intent = new Intent(getContext(),ScrollingActivity.class);
                intent.putExtra("name", itemsConstructorPop.get(position).name);
                intent.putExtra("unit_price", itemsConstructorPop.get(position).unit_price);
                intent.putExtra("customers_idcustomers", itemsConstructorPop.get(position).customers_idcustomers);
                intent.putExtra("description", itemsConstructorPop.get(position).description);
                intent.putExtra("stock", itemsConstructorPop.get(position).stock);
                intent.putExtra("image_url",itemsConstructorPop.get(position).image);
                intent.putExtra("products_idproducts",itemsConstructorPop.get(position).id);
                startActivity(intent);
            }
        });


        ref.child("counter").child("0").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                idproducts = Integer.parseInt((String) snapshot.child("idproducts").getValue());
                itemsConstructorPop.removeAll(itemsConstructorPop);
                for (int i=0; i<idproducts;i++) {
                    ref.child("products").child(i + "").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            name = (String) snapshot.child("name").getValue();
                            unit_price = (String) snapshot.child("unit_price").getValue();
                            customers_idcustomers = (String) snapshot.child("customers_idcustomers").getValue();
                            description = (String) snapshot.child("description").getValue();
                            stock = (String) snapshot.child("stock").getValue();
                            id = (String) snapshot.child("idproducts").getValue();

                            itemsConstructorPop.add(new ItemsConstructorPop(id, customers_idcustomers,name, unit_price, R.drawable.tesla_model_x, description, stock));
                            itemsAdapterPop = new ItemsAdapterPop(getContext(), itemsConstructorPop);
                            listView.setAdapter(itemsAdapterPop);
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {
                            System.out.println("The read failed: " + firebaseError.getMessage());
                        }
                    });
                }
                progressView.setVisibility(View.GONE);
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        });

        // Inflate the layout for this fragment
        return view;

    }

}
