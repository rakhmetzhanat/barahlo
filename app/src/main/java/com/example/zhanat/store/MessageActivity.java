package com.example.zhanat.store;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.zhanat.store.Adapter.MessagesAdapter;
import com.example.zhanat.store.Constructor.MessagesConstructor;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;

public class MessageActivity extends AppCompatActivity {

    ArrayList<MessagesConstructor> messagesConstructor = new ArrayList<MessagesConstructor>();
    MessagesAdapter messagesAdapter;
    EditText editText8;
    ImageButton imageButton;

    public Firebase ref;

    public String customers_idcustomers;
    public String users_idusers;
    public String from;
    public String content;
    public String read;

    int idmessage;
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MyPrefs";

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(MessageActivity.this);
        setContentView(R.layout.activity_message);

        ref = new Firebase("https://flickering-heat-3014.firebaseio.com");

        editText8 = (EditText) findViewById(R.id.editText8);
        imageButton = (ImageButton) findViewById(R.id.imagebutton);


        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!editText8.getText().toString().equals("") && editText8.getText().toString()!="")
                {


                    ref.child("counter").child("0").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            idmessage = Integer.parseInt((String) snapshot.child("idmessage").getValue());
                            messagesConstructor.removeAll(messagesConstructor);
                            sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                            customers_idcustomers = getIntent().getExtras().getString("customers_idcustomers");

                            ref.child("counter").child("0").child("idmessage").setValue((idmessage + 1) + "");
                            ref.child("message").child(idmessage + "").child("content").setValue(editText8.getText().toString());
                            ref.child("message").child(idmessage + "").child("users_idusers").setValue(sharedPreferences.getString("users_idusersKey",""));
                            ref.child("message").child(idmessage + "").child("customers_idcustomers").setValue(customers_idcustomers);
                            ref.child("message").child(idmessage + "").child("from").setValue("user");
                            ref.child("message").child(idmessage + "").child("read").setValue("1");

                            editText8.setText("");
                        }
                        @Override
                        public void onCancelled(FirebaseError firebaseError) {
                            System.out.println("The read failed: " + firebaseError.getMessage());
                        }
                    });

                    onResume();

                }


            }});


        listView = (ListView) findViewById(R.id.listMess);

    }


    @Override
    public void onResume() {


        ref.child("counter").child("0").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                idmessage = Integer.parseInt((String) snapshot.child("idmessage").getValue());
                messagesConstructor.removeAll(messagesConstructor);

                for(int j=idmessage; j>=0; j--)
                {
                    ref.child("message").child(j + "").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                            customers_idcustomers = getIntent().getExtras().getString("customers_idcustomers");

                            if(sharedPreferences.getString("users_idusersKey","").equals((String) snapshot.child("users_idusers").getValue()) && customers_idcustomers.equals((String) snapshot.child("customers_idcustomers").getValue()))
                            {

                                customers_idcustomers = (String) snapshot.child("customers_idcustomers").getValue();
                                users_idusers = (String) snapshot.child("users_idusers").getValue();
                                from = (String) snapshot.child("from").getValue();
                                content = (String) snapshot.child("content").getValue();
                                read = (String) snapshot.child("read").getValue();

                                messagesConstructor.add(new MessagesConstructor(customers_idcustomers, users_idusers, from, content, read));
                                messagesAdapter = new MessagesAdapter(MessageActivity.this, messagesConstructor);
                                listView.setAdapter(messagesAdapter);

                            }
                        }
                        @Override
                        public void onCancelled(FirebaseError firebaseError) {
                            System.out.println("The read failed: " + firebaseError.getMessage());
                        }
                    });
                }
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        });

        super.onResume();

    }


}
