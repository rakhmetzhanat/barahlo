package com.example.zhanat.store.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.zhanat.store.LoginActivity;
import com.example.zhanat.store.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMain extends Fragment {

    TextView textView, textView11, textView13, textView15, textView17;
    Button button2;

    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    public FragmentMain() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_main, container, false);

        sharedPreferences = this.getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        textView = (TextView) view.findViewById(R.id.textView);
        textView11 = (TextView) view.findViewById(R.id.textView11);
        textView13 = (TextView) view.findViewById(R.id.textView13);
        textView15 = (TextView) view.findViewById(R.id.textView15);
        textView17 = (TextView) view.findViewById(R.id.textView17);

        button2 = (Button) view.findViewById(R.id.button2);

        textView.setText(sharedPreferences.getString("nameKey","") + " " + sharedPreferences.getString("surnameKey",""));
        textView11.setText(sharedPreferences.getString("emailKey",""));
        textView13.setText(sharedPreferences.getString("phoneKey",""));
        textView15.setText(sharedPreferences.getString("passwordKey",""));
        textView17.setText(sharedPreferences.getString("users_idusersKey",""));

        return  view;
    }

    @Override
    public void onResume() {
        super.onResume();

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedPreferences.edit().clear().commit();
                getActivity().finish();
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

    }

}
