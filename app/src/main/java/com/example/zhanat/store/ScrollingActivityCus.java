package com.example.zhanat.store;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.github.rahatarmanahmed.cpv.CircularProgressView;

public class ScrollingActivityCus extends AppCompatActivity {
    FloatingActionButton fab;
    public Firebase ref;

    TextView textView22, textView23, textView24, textView25;
    CircularProgressView progressView;

    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    String loginKey;

    int idcustomers;
    String name;
    String surname;
    String email;
    String telephone;
    String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(ScrollingActivityCus.this);
        setContentView(R.layout.activity_scrolling_activity_cus);

        ref = new Firebase("https://flickering-heat-3014.firebaseio.com");
        progressView = (CircularProgressView) findViewById(R.id.progress_view);

        fab = (FloatingActionButton) findViewById(R.id.fabs);

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        loginKey = sharedPreferences.getString("loginKey","");

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(loginKey.equals("true"))
                {
                    Intent intent = new Intent(ScrollingActivityCus.this,MessageActivity.class);
                    intent.putExtra("customers_idcustomers", getIntent().getExtras().getString("customers_idcustomers"));
                    startActivity(intent);
                }else
                {
                    Toast toast = Toast.makeText(ScrollingActivityCus.this, "Вы не зарегистрированы в Barahlo", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });


        textView22 = (TextView) findViewById(R.id.textView22);
        textView23 = (TextView) findViewById(R.id.textView23);
        textView24 = (TextView) findViewById(R.id.textView24);
        textView25 = (TextView) findViewById(R.id.textView25);

        ref.child("counter").child("0").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                idcustomers = Integer.parseInt((String) snapshot.child("idcustomers").getValue());

                for(int j=0; j<idcustomers; j++)
                {
                    ref.child("customers").child(j + "").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            if(getIntent().getExtras().getString("customers_idcustomers").equals((String) snapshot.child("idcustomers").getValue()))
                            {
                                name = (String) snapshot.child("name").getValue();
                                surname = (String) snapshot.child("surname").getValue();
                                email = (String) snapshot.child("email").getValue();
                                telephone = (String) snapshot.child("telephone").getValue();
                                address = (String) snapshot.child("address").getValue();

                                textView22.setText(name + " " + surname);
                                textView23.setText(email);
                                textView24.setText(telephone);
                                textView25.setText(address);
                            }
                        }
                        @Override
                        public void onCancelled(FirebaseError firebaseError) {
                            System.out.println("The read failed: " + firebaseError.getMessage());
                        }
                    });
                }
                progressView.setVisibility(View.GONE);
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        });

    }
}
