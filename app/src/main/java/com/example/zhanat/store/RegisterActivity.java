package com.example.zhanat.store;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;


public class RegisterActivity extends AppCompatActivity{

    private Button RegButton;
    private EditText surname, name, phone, email, password;

    private String f_email;
    private int idusers;
    private Firebase ref;
    private int check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(RegisterActivity.this);
        ref = new Firebase("https://flickering-heat-3014.firebaseio.com");
        setContentView(R.layout.activity_register);


        surname = (EditText) findViewById(R.id.editText);
        name = (EditText) findViewById(R.id.editText2);
        phone = (EditText) findViewById(R.id.editText5);
        email = (EditText) findViewById(R.id.editText3);
        password = (EditText) findViewById(R.id.editText4);


        RegButton = (Button) findViewById(R.id.button3);
        RegButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

    }


    private void attemptLogin() {

        surname.setError(null);
        name.setError(null);
        phone.setError(null);
        email.setError(null);
        password.setError(null);

        final String surname_str = surname.getText().toString();
        final String name_str = name.getText().toString();
        final String phone_str= phone.getText().toString();
        final String email_str = email.getText().toString();
        final String password_str = password.getText().toString();


        if(TextUtils.isEmpty(surname_str))
        {
            surname.setError("Пустое поле");
        }
        else if (TextUtils.isEmpty(name_str)) {
            name.setError("Пустое поле");
        }
        else if (TextUtils.isEmpty(phone_str)) {
            phone.setError("Пустое поле");
        }
        else if (!isPhoneValid(phone_str)) {
            phone.setError("Пример: 87000000000");
        }
        else if (TextUtils.isEmpty(email_str)) {
            email.setError("Пустое поле");
        }
        else if (!isEmailValid(email_str)) {
            email.setError("Пример: example@example.com");
        }
        else if (TextUtils.isEmpty(password_str)) {
            password.setError("Пустое поле");
        }
        else if (!isPasswordValid(password_str)) {
            password.setError("Не менее 6 элементов");
        }else
        {
            ref.child("counter").child("0").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    idusers = Integer.parseInt((String) snapshot.child("idusers").getValue());
                    for (int i = 0; i < idusers; i++) {
                        ref.child("users").child(i + "").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {
                                f_email = (String) snapshot.child("email").getValue();

                                if (!f_email.equals(email_str) && f_email != email_str) {
                                    ref.child("counter").child("0").child("idusers").setValue((idusers + 1) + "");
                                    ref.child("users").child(idusers + "").child("users_idusers").setValue(idusers + "");
                                    ref.child("users").child(idusers + "").child("name").setValue(name_str);
                                    ref.child("users").child(idusers + "").child("surname").setValue(surname_str);
                                    ref.child("users").child(idusers + "").child("phone").setValue(phone_str);
                                    ref.child("users").child(idusers + "").child("email").setValue(email_str);
                                    ref.child("users").child(idusers + "").child("password").setValue(password_str);

                                    Toast toast = Toast.makeText(RegisterActivity.this ,"Регистрация успешно завершена!", Toast.LENGTH_SHORT);
                                    toast.show();

                                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    RegisterActivity.this.finish();
                                }
                                else if (f_email.equals(email_str) && f_email == email_str) {
                                    Toast toast = Toast.makeText(RegisterActivity.this ,"Пользователь с таким Eemail уже существует!", Toast.LENGTH_SHORT);
                                    toast.show();
                                }
                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {
                                System.out.println("The read failed: " + firebaseError.getMessage());
                            }
                        });
                    }
                }
                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.out.println("The read failed: " + firebaseError.getMessage());
                }
            });
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 6;
    }
    private boolean isPhoneValid(String password) {
        return password.length() == 11;
    }
    private boolean isEmailValid(String email) {
        return email.contains("@");
    }


}