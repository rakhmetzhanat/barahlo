package com.example.zhanat.store;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.zhanat.store.Fragment.FragmentAbout;
import com.example.zhanat.store.Fragment.FragmentBusket;
import com.example.zhanat.store.Fragment.FragmentMain;
import com.example.zhanat.store.Fragment.FragmentSettings;
import com.firebase.client.Firebase;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FragmentTabs fragmentTabs;
    FragmentMain fragmentMain;
    FragmentBusket fragmentBusket;
    FragmentSettings fragmentSettings;
    FragmentAbout fragmentAbout;

    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Firebase.setAndroidContext(this);
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null)
        {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTabs = new FragmentTabs();
            fragmentTransaction.replace(R.id.container, fragmentTabs).commit();
        }
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fragmentMain = new FragmentMain();
        fragmentBusket = new FragmentBusket();
        fragmentSettings = new FragmentSettings();
        fragmentAbout = new FragmentAbout();

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        String loginKey = sharedPreferences.getString("loginKey","");

        int id = item.getItemId();

//        getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentById(R.id.container)).commit();

        if (id == R.id.nav_main) {

            fragmentTransaction.replace(R.id.container, fragmentTabs).commit();
        } else if (id == R.id.nav_profile) {
            if(loginKey.equals("true"))
            {
                fragmentTransaction.replace(R.id.container, fragmentMain).commit();
            }else
            {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
            }
        } else if (id == R.id.nav_busket) {
            if(loginKey.equals("true"))
            {
                fragmentTransaction.replace(R.id.container, fragmentBusket).commit();
            }else
            {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
            }
        } else if (id == R.id.nav_settings) {
            fragmentTransaction.replace(R.id.container, fragmentSettings).commit();
        } else if (id == R.id.nav_about) {
            fragmentTransaction.replace(R.id.container, fragmentAbout).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
