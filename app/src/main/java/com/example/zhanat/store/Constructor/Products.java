package com.example.zhanat.store.Constructor;

/**
 * Created by zhanat on 4/22/16.
 */
public class Products
{

    private String name;
    private String unit_price;

    public Products(String name, String unit_price) {
        this.name = name;
        this.unit_price = unit_price;
    }

    public String getName() {
        return name;
    }

    public String getUnit_price() {
        return unit_price;
    }
}
