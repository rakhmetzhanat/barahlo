package com.example.zhanat.store;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class ScrollingActivity extends AppCompatActivity{
    TextView textView4,textView5, textView6, textView7;
    ImageView imageView;
    Button button2;
    public Firebase ref;
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    String loginKey;
    String products_idproducts;
    String users_idusers;
    int idboard;
    FloatingActionButton fab;
    int j;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(ScrollingActivity.this);
        ref = new Firebase("https://flickering-heat-3014.firebaseio.com");


        setContentView(R.layout.activity_scrolling);

        textView4 = (TextView) findViewById(R.id.textView4);
        textView5 = (TextView) findViewById(R.id.textView5);
        textView6 = (TextView) findViewById(R.id.textView6);
        textView7 = (TextView) findViewById(R.id.textView8);
        imageView = (ImageView) findViewById(R.id.toolbar);

        button2 = (Button) findViewById(R.id.button2);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        imageView.setImageResource(getIntent().getExtras().getInt("image_url"));
        textView6.setText(getIntent().getExtras().getString("description"));
        textView4.setText(getIntent().getExtras().getString("name"));
        textView5.setText(getIntent().getExtras().getString("unit_price"));
        textView7.setText(getIntent().getExtras().getString("stock"));

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        loginKey = sharedPreferences.getString("loginKey","");

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ScrollingActivity.this,ScrollingActivityCus.class);
                intent.putExtra("customers_idcustomers", getIntent().getExtras().getString("customers_idcustomers"));
                intent.putExtra("products_idproducts",getIntent().getExtras().getString("products_idproducts"));
                startActivity(intent);
            }
        });

        if(loginKey.equals("true")) {

            ref.child("counter").child("0").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    idboard = Integer.parseInt((String) snapshot.child("idboard").getValue());
                    products_idproducts = getIntent().getExtras().getString("products_idproducts");
                    for (int j = 0; j < idboard; j++) {
                        ref.child("board").child(j + "").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {
                                sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                users_idusers = sharedPreferences.getString("users_idusersKey", "");
                                products_idproducts = getIntent().getExtras().getString("products_idproducts");

                                if (!users_idusers.equals((String) snapshot.child("users_idusers").getValue()) && !products_idproducts.equals((String) snapshot.child("products_idproducts").getValue())) {
                                    fab.setImageResource(R.drawable.heart);
                                }
                                else if (users_idusers.equals((String) snapshot.child("users_idusers").getValue()) && products_idproducts.equals((String) snapshot.child("products_idproducts").getValue())) {
                                    fab.setImageResource(R.drawable.favorite);
                                }
                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {
                                System.out.println("The read failed: " + firebaseError.getMessage());
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.out.println("The read failed: " + firebaseError.getMessage());
                }
            });
        }else
        {
            Toast toast = Toast.makeText(ScrollingActivity.this, "Вы не зарегистрированы в Barahlo", Toast.LENGTH_SHORT);
            toast.show();
        }


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(loginKey.equals("true")) {

                    ref.child("counter").child("0").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            idboard = Integer.parseInt((String) snapshot.child("idboard").getValue());
                            products_idproducts = getIntent().getExtras().getString("products_idproducts");

                            for (j = 0; j < idboard; j++) {
                                ref.child("board").child(j + "").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshot) {
                                        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                        users_idusers = sharedPreferences.getString("users_idusersKey", "");
                                        products_idproducts = getIntent().getExtras().getString("products_idproducts");

                                        if (!users_idusers.equals((String) snapshot.child("users_idusers").getValue()) && !products_idproducts.equals((String) snapshot.child("products_idproducts").getValue())) {
                                            fab.setImageResource(R.drawable.favorite);
                                            ref.child("counter").child("0").child("idboard").setValue((idboard + 1) + "");
                                            ref.child("board").child(idboard + "").child("idboard").setValue(idboard + "");
                                            ref.child("board").child(idboard + "").child("users_idusers").setValue(users_idusers);
                                            ref.child("board").child(idboard + "").child("products_idproducts").setValue(products_idproducts);
                                        }
                                        else if (users_idusers.equals((String) snapshot.child("users_idusers").getValue()) && products_idproducts.equals((String) snapshot.child("products_idproducts").getValue())) {
                                            fab.setImageResource(R.drawable.heart);
                                            ref.child("counter").child("0").child("idboard").setValue((idboard - 1) + "");

                                            ref.child("board").child((j-1) + "").child("idboard").removeValue();
                                            ref.child("board").child((j-1) + "").child("users_idusers").removeValue();
                                            ref.child("board").child((j-1) + "").child("products_idproducts").removeValue();

                                            ref.child("board").child((j) + "").child("idboard").removeValue();
                                            ref.child("board").child((j) + "").child("users_idusers").removeValue();
                                            ref.child("board").child((j) + "").child("products_idproducts").removeValue();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(FirebaseError firebaseError) {
                                        System.out.println("The read failed: " + firebaseError.getMessage());
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {
                            System.out.println("The read failed: " + firebaseError.getMessage());
                        }
                    });

                }else
                {
                    Toast toast = Toast.makeText(ScrollingActivity.this, "Вы не зарегистрированы в Barahlo", Toast.LENGTH_SHORT);
                    toast.show();
                }


            }//
        });

    }
}
