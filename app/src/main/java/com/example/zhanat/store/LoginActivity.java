package com.example.zhanat.store;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;


public class LoginActivity extends AppCompatActivity{

    private Firebase ref;
    private Button LogButton, RegButton;
    private EditText email, password;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Name = "nameKey";
    public static final String Surname = "surnameKey";
    public static final String Phone = "phoneKey";
    public static final String Email = "emailKey";
    public static final String Password = "passwordKey";
    public static final String UsersID = "users_idusersKey";
    public static final String Login = "loginKey";

    String name;
    String phone;
    String surname;
    String userID;

    private String f_email;
    private String f_password;

    private int idusers;
    private int check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(LoginActivity.this);
        ref = new Firebase("https://flickering-heat-3014.firebaseio.com");
        setContentView(R.layout.activity_login);

        sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        email = (EditText) findViewById(R.id.editText6);
        password = (EditText) findViewById(R.id.editText7);

        LogButton = (Button) findViewById(R.id.button4);
        LogButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        RegButton = (Button) findViewById(R.id.button5);
        RegButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginActivity.this.finish();
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

    }

    private void attemptLogin() {

        email.setError(null);
        password.setError(null);

        final String email_str = email.getText().toString();
        final String password_str = password.getText().toString();

        if (TextUtils.isEmpty(email_str)) {
            email.setError("Пустое поле");
        }
        else if (!isEmailValid(email_str)) {
            email.setError("Пример: example@example.com");
        }
        else if (TextUtils.isEmpty(password_str)) {
            password.setError("Пустое поле");
        }
        else if (!isPasswordValid(password_str)) {
            password.setError("Не менее 6 элементов");
        }else
        {
            LogButton.setVisibility(View.INVISIBLE);
            ref.child("counter").child("0").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    idusers = Integer.parseInt((String) snapshot.child("idusers").getValue());
                    for (int i = 0; i < idusers; i++) {
                        ref.child("users").child(i + "").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {

                                f_email = (String) snapshot.child("email").getValue();
                                f_password = (String) snapshot.child("password").getValue();

                                if (!f_email.equals(email_str) && !f_password.equals(password_str)) {
                                    check = 0;
                                    editor = sharedPreferences.edit();
                                    editor.putString(Login, "false");
                                    editor.commit();
                                }
                                else if (f_email.equals(email_str) && f_password.equals(password_str)) {
                                    check = 1;
                                    userID= (String) snapshot.child("users_idusers").getValue();
                                    name = (String) snapshot.child("name").getValue();
                                    phone = (String) snapshot.child("phone").getValue();
                                    surname = (String) snapshot.child("surname").getValue();

                                    editor = sharedPreferences.edit();

                                    editor.putString(Email, email_str);
                                    editor.putString(Name, name);
                                    editor.putString(Phone, phone);
                                    editor.putString(Surname, surname);
                                    editor.putString(UsersID, userID);
                                    editor.putString(Password, f_password);

                                    editor.putString(Login, "true");
                                    editor.commit();

                                    Toast toast = Toast.makeText(LoginActivity.this , name + " , вы успешно вошли в систему!", Toast.LENGTH_SHORT);
                                    toast.show();

                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    LoginActivity.this.finish();
                                }
                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {
                                System.out.println("The read failed: " + firebaseError.getMessage());
                            }
                        });
                    }
                    LogButton.setVisibility(View.VISIBLE);
                }
                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.out.println("The read failed: " + firebaseError.getMessage());
                }
            });

            if(check==0)
            {
                Toast toast = Toast.makeText(LoginActivity.this, "Не правильный пароль: " + password_str + "\n" + "или Email: " + email_str, Toast.LENGTH_SHORT);
                toast.show();
            }else if(check==1)
            {
                Toast toast = Toast.makeText(LoginActivity.this , name + " , вы успешно вошли в систему!", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 6;
    }
    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

}