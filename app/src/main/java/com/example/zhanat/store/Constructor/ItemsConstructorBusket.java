package com.example.zhanat.store.Constructor;

/**
 * Created by zhanat on 4/21/16.
 */
public class ItemsConstructorBusket {
    public String name;
    public String unit_price;
    public String customers_idcustomers;
    public String description;
    public String stock;
    public int image;
    public String id;

    public ItemsConstructorBusket(String ids,String customers_idcustomerss, String names, String unit_prices, int images, String descriptions, String stocks) {
        name = names;
        unit_price = unit_prices;
        description = descriptions;
        stock = stocks;
        image = images;
        customers_idcustomers = customers_idcustomerss;
        id = ids;
    }
}
