package com.example.zhanat.store.Adapter;
/**
 * Created by zhanat on 4/14/16.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zhanat.store.Constructor.ItemsConstructor;
import com.example.zhanat.store.R;

import java.util.ArrayList;

public class ItemsAdapter extends BaseAdapter {
    TextView textView2, textView3;
    ImageView imageView2;
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<ItemsConstructor> objects;

    public ItemsAdapter(Context context, ArrayList<ItemsConstructor> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = lInflater.inflate(R.layout.items, parent, false);
        if (view == null) {
            view = lInflater.inflate(R.layout.items, parent, false);
        }

        ItemsConstructor itemsConstructor = getItemsConstructor(position);

        textView2 = (TextView) view.findViewById(R.id.textView2);
        textView2.setText(itemsConstructor.name);

        textView3 = (TextView) view.findViewById(R.id.textView3);
        textView3.setText(itemsConstructor.unit_price);


        imageView2 = (ImageView) view.findViewById(R.id.imageView2);
        imageView2.setImageResource(itemsConstructor.image);

        return view;
    }

    ItemsConstructor getItemsConstructor(int position) {
        return ((ItemsConstructor) getItem(position));
    }
}